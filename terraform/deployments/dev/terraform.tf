terraform {
  backend "s3" {
//    bucket = "${var.state_bucket}"
//    dynamodb_table = "${var.dynamo_table_name}"
    key     = "dev/poc-hcm-adapter-lambda/terraform.tfstate"
    region  = "eu-west-1"
    encrypt = true

  }
}

data "terraform_remote_state" "account" {
  backend = "s3"
  workspace = "${var.environment}"

  config {
    bucket   = "${var.state_bucket}"
    key      = "${var.environment}/terraform.tfstate"
    region   = "${var.region}"
    profile  = "${var.profile}"
  }
}
