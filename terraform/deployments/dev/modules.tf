module "hcm_adapter" {
  source          = "../../modules/hcm_adapter"
  lambda_filepath = "../../../target/poc-hcm-adapter-lambda-1.0-SNAPSHOT.jar"
  environment     = "${var.environment}"
  policy_read_write_access_to_extract_bucket_arn  = "${data.terraform_remote_state.account.policy_read_write_access_to_extract_bucket_arn}"
  policy_read_write_access_to_upload_bucket_arn   = "${data.terraform_remote_state.account.policy_read_write_access_to_upload_bucket_arn}"
  upload_bucket_id   = "${data.terraform_remote_state.account.upload_bucket_id}"
  upload_bucket_arn  = "${data.terraform_remote_state.account.upload_bucket_arn}"
}

module "ecs_task" {
  source             = "../../modules/ecs_task"
  environment        = "${var.environment}"
  ecr_image          = "${var.ecr_image}"
  policy_read_write_access_to_extract_bucket_arn  = "${data.terraform_remote_state.account.policy_read_write_access_to_extract_bucket_arn}"
  policy_read_write_access_to_upload_bucket_arn   = "${data.terraform_remote_state.account.policy_read_write_access_to_upload_bucket_arn}"
}