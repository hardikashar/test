variable "region" {
  default = "eu-west-1"
}

variable "state_bucket" {}

variable "profile" {}

## General
variable "environment" {}


variable "cluster" {
   description = "The name of the cluster"
}

variable "aws_ami" {}

variable "instance_type" {
  description = "The AWS ami id to use"
}

variable "security_group_id" {}


variable "vpc_id" {
  description = "The VPC id"
}

variable "private_subnet_ids" {
  type        = "list"
  description = "The list of private subnets to place the instances in"
}

variable "ecr_image" {
  description = "ECR image name to be used by ECS"
}
