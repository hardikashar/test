#!/usr/bin/env bash
modules=$(find . -type f -name "*.tf" -exec dirname {} \;|sort -u)
for m in ${modules}
do
(terraform validate $1 "$m" && echo "√ $m") || exit 1
done
