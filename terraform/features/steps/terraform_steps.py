from behave import *
from botocore.client import ClientError
import time


def aws_client_exception(f):
    def applicator(*args, **kwargs):
        try:
            f(*args, **kwargs)
        except ClientError:
            assert False
    return applicator


# Lambda
@given('the lambda function "{function_name}" is created')
@aws_client_exception
def is_lambda_created(context, function_name):
    context.function_name = function_name
    context.response = context.lambda_client.get_function(FunctionName=function_name)
    assert context.response['Configuration']['FunctionName'] is not None


@then('the allocated memory is "{memory}" MB')
def check_lambda_memory(context, memory):
    assert context.response['Configuration']['MemorySize'] == int(memory)


@then('the timeout is of "{timeout}" seconds')
def check_lambda_timeout(context, timeout):
    assert context.response['Configuration']['Timeout'] == int(timeout)


# Bucket
@given('the s3 bucket "{bucket_name}" is created')
@aws_client_exception
def is_bucket_created(context, bucket_name):
    context.bucket_name = bucket_name
    context.s3_client.head_bucket(Bucket=bucket_name)
    assert True


@then('the s3 bucket "{bucket_name}" is not public')
@aws_client_exception
def is_bucket_public(context, bucket_name):
    context.response = context.s3_client.get_bucket_acl(Bucket=bucket_name)
    for grant in context.response['Grants']:
        if grant['Grantee']['Type'].lower() == 'group' \
                and grant['Grantee']['URI'] == 'http://acs.amazonaws.com/groups/global/AllUsers':
            assert False
    assert True


# e2e
@given('the s3 bucket "{bucket_name}" is empty')
@aws_client_exception
def ensure_bucket_empty(context, bucket_name):
    context.empty_bucket(bucket_name)
    assert True


@when('I execute the lambda function')
@aws_client_exception
def execute_lambda(context):
    context.response = context.lambda_client.invoke(
        FunctionName=context.function_name,
        InvocationType='Event'
    )
    assert context.response['StatusCode'] == 202


@then('object "{object_name}" is created in bucket')
def is_object_created(context, object_name,):
    total_time = 0
    while total_time < 60:
        try:
            context.s3_client.head_object(Bucket=context.bucket_name, Key=object_name)
            break
        except ClientError:
            time.sleep(5)
            total_time = total_time + 5
    assert total_time < 60
    time.sleep(60)
