import boto3
from functools import partial
from botocore.client import ClientError


def _empty_bucket(context, bucket):
    try:
        for obj in context.s3_client.list_objects_v2(Bucket=context.bucket_name)['Contents']:
            context.s3_client.delete_object(Bucket=bucket, Key=obj['Key'])
    except (ClientError, KeyError):
        pass


def _get_boto_session():
    return boto3.session.Session(profile_name='terraformuser')


def before_all(context):
    boto_session = _get_boto_session()
    context.lambda_client = boto_session.client('lambda')
    context.s3_client = boto_session.client('s3')
    context.empty_bucket = partial(_empty_bucket, context)


def after_tag(context, tag):
    if tag == "empty_bucket":
        context.empty_bucket(context.bucket_name)
        return
