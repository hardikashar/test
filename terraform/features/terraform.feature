Feature: Terraform resources and integration tests

  @dev
  Scenario Outline: Check if lambda functions are created using Terraform
    Given the lambda function "<function_name>" is created
    Then the allocated memory is "<memory>" MB
    And the timeout is of "<timeout>" seconds

    Examples: functions
      | function_name            | memory | timeout |
      | dev_POCAdapterLambda_HCM | 512    | 300     |

  @dev
  Scenario Outline: Check if buckets are created using Terraform
    Given the s3 bucket "<bucket_name>" is created
    Then the s3 bucket "<bucket_name>" is not public

    Examples: buckets
      | bucket_name             |
      | peacock-poc-hcm-upload  |
      | peacock-poc-hcm-extract |
