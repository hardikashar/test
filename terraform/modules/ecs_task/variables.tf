variable "environment" {
  description = "The name of the environment"
}

variable "region" {
  default = "eu-west-1"
}

variable "ecr_image" {
  description = "ECR image name to be used by ECS"
}

variable "policy_read_write_access_to_extract_bucket_arn" {}

variable "policy_read_write_access_to_upload_bucket_arn" {}