data "template_file" "adapter_task_template" {
  template = "${file("${path.module}/data/task_definition.json")}"
  vars {
    ecr_image    = "${var.ecr_image}"
    log_group    = "${aws_cloudwatch_log_group.ecs_task_hcm.name}"
    region       = "${var.region}"
  }
}


resource "aws_ecs_task_definition" "adapter_task_HCM" {
//  family                = "${var.environment}_adapter_task"
  family                = "peacock-ecs-poc-hcm-task"
  container_definitions = "${data.template_file.adapter_task_template.rendered}"
  task_role_arn         = "${aws_iam_role.ecs_task_instance_role_hcm.arn}"
}


resource "aws_cloudwatch_log_group" "ecs_task_hcm" {
  name              = "${var.environment}/ecs/adapter_task_hcm"
  retention_in_days = 30
}