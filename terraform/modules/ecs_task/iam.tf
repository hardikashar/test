resource "aws_iam_role" "ecs_task_instance_role_hcm" {
  name = "${var.environment}_ecs_task_instance_role_hcm"

  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Sid": "",
      "Effect": "Allow",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
      "Action": "sts:AssumeRole"
    }
  ]
}
EOF
}


resource "aws_iam_role_policy_attachment" "read_write_access_to_extract_bucket_policy_attachment" {
  policy_arn = "${var.policy_read_write_access_to_extract_bucket_arn}"
  role      = "${aws_iam_role.ecs_task_instance_role_hcm.name}"
}

resource "aws_iam_role_policy_attachment" "read_write_access_to_upload_bucket_policy_attachment" {
  policy_arn = "${var.policy_read_write_access_to_upload_bucket_arn}"
  role      = "${aws_iam_role.ecs_task_instance_role_hcm.name}"
}