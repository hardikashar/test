variable "lambda_filepath" {}

variable "environment" {}

variable "policy_read_write_access_to_extract_bucket_arn" {}

variable "policy_read_write_access_to_upload_bucket_arn" {}

variable "upload_bucket_id" {
  description = "ID of bucket to upload peacock files"
}

variable "upload_bucket_arn" {
  description = "ARN of bucket to upload peacock files"
}