resource "aws_iam_role" "iam_for_hcm_lamda" {
  name               = "iam_for_hcm_lamda"
  assume_role_policy = "${file("${path.module}/data/aws_iam_role.lamda_adapter_ecr.policy")}"
}


resource "aws_iam_role_policy" "ecs_cloudwatch_access_for_lambda_HCM" {
  name        = "lambda_policy_2"
  policy      = "${file("${path.module}/data/aws_iam_role.lamda_adapter_cloudwatch.policy")}"
  role        = "${aws_iam_role.iam_for_hcm_lamda.id}"
}


resource "aws_iam_role_policy_attachment" "read_write_access_to_extract_bucket_policy_attachment" {
  policy_arn = "${var.policy_read_write_access_to_extract_bucket_arn}"
  role      = "${aws_iam_role.iam_for_hcm_lamda.name}"
}

resource "aws_iam_role_policy_attachment" "read_write_access_to_upload_bucket_policy_attachment" {
  policy_arn = "${var.policy_read_write_access_to_upload_bucket_arn}"
  role      = "${aws_iam_role.iam_for_hcm_lamda.name}"
}
