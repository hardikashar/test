resource "aws_lambda_function" "app_adapter_lambda_hcm" {
  filename         = "${var.lambda_filepath}"
  function_name    =  "${var.environment}_POCAdapterLambda_HCM"
  role             = "${aws_iam_role.iam_for_hcm_lamda.arn}"
  handler          = "com.morrisons.peacock.hcm.adapter.poc.lambda.PocHCMAdapterLambda::handleRequest"
  source_code_hash = "${base64sha256(file("${var.lambda_filepath}"))}"
  runtime          = "java8"
  memory_size      =  "512"
  timeout          =  "300"
  environment {
    variables = {
      OPERATION = "upload"
    }
  }
}

resource "aws_lambda_permission" "allow_bucket" {
  statement_id  = "AllowExecutionFromS3Bucket"
  action        = "lambda:InvokeFunction"
  function_name = "${aws_lambda_function.app_adapter_lambda_hcm.arn}"
  principal     = "s3.amazonaws.com"
  source_arn    = "${var.upload_bucket_arn}"
}



resource "aws_s3_bucket_notification" "bucket_notification" {
  bucket = "${var.upload_bucket_id}"

  lambda_function {
    lambda_function_arn = "${aws_lambda_function.app_adapter_lambda_hcm.arn}"
    events              = ["s3:ObjectCreated:*"]
  }
}



