package morrisons.domains.online
HIIIIIIIIIIIIIIIIIIIIII


job("10_peacock-hcm-adapter_v1_build_master") {
	description("This job pulls code from morrisonsplc/hcm-adapter bucket and builds the code.After Successful build it archives .jar file, Docker file , config/* and aws_json/**")
	keepDependencies(false)
	label('master')
	scm {
		git {
			branch("*/feature/*")
			remote {
				url('git@bitbucket.org:morrisonsplc/poc-hcm-adapter-lambda.git')
				// credentials("21c92be8-46e7-4720-83d4-7c3c45386cb4")
			}
		}
	}


	disabled(false)
	concurrentBuild(false)


	steps {

		maven {
			mavenInstallation('apache-maven-3.0.5')
			goals("""clean
				package""")
			rootPOM('pom.xml')
		}
	}

	publishers {
		archiveArtifacts {
			pattern("target/poc-hcm-adapter-lambda-*.jar,Dockerfile,config/*,aws_json/**")
			allowEmpty(false)
			onlyIfSuccessful(false)
			fingerprint(false)
			defaultExcludes(true)
		}
	}

	wrappers {
		preBuildCleanup {
			deleteDirectories(false)
			cleanupParameter()
		}
	}

	configure {
		it / 'properties' / 'jenkins.model.BuildDiscarderProperty' {
			strategy {
				'daysToKeep'('3')
				'numToKeep'('3')
				'artifactDaysToKeep'('-1')
				'artifactNumToKeep'('-1')
			}
		}
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
	}


	publishers {
		downstream('20_peacock-hcm-adapter_v1_build_container', 'SUCCESS')
		wsCleanup()
	}
}

job("20_peacock-hcm-adapter_v1_build_container") {
	description("This Jenkins Project copies artifacts from FoodToOrder_Basket_Fetch_And_Build_Code Project.It builds the Docker Image using the artifacts copied, and finally pushes the Docker Image to Docker Hub.")
	keepDependencies(false)
	disabled(false)
	concurrentBuild(false)
	label('docker-build')



	steps {
		copyArtifacts('10_peacock-hcm-adapter_v1_build_master') {
			includePatterns('Dockerfile,**/config/*.yml,target/basket-service-1.0.jar')
			flatten(true)
			buildSelector { latestSuccessful(false) }
		}
	}



	steps { shell("""#!/bin/bash
set -e

mkdir -p app/bin
mkdir -p app/bin/config
mkdir -p app/logs

servicename=basket;
targetenv=dev;

# And the application jar.
mv \${"servicename"}-service-1.0.jar app/bin

# And the config.yml.
mv *.yml app/bin/config

# If image already exists remove it.
# For this one job only we hijack the remittance docker hub so name tst tag as tst_awstest.
repository=morrisons/\${"servicename"}-service;

# Re-build image and push to docker hub.
docker build -t \${"repository"}:\${"targetenv"} .;
docker push \${"repository"}:\${"targetenv"};

#Remove DEV Image
docker rmi \${"repository"}:\${"targetenv"}""") }


	wrappers {
		preBuildCleanup {
			deleteDirectories(false)
			cleanupParameter()
		}
	}


	configure {
		it / 'properties' / 'jenkins.model.BuildDiscarderProperty' {
			strategy {
				'daysToKeep'('3')
				'numToKeep'('3')
				'artifactDaysToKeep'('-1')
				'artifactNumToKeep'('-1')
			}
		}
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
		it / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
			projectNameList { string('10_peacock-hcm-adapter_v1_build_master') }
		}
	}

	publishers {
		downstream('30_peacock-hcm-adapter_v1_deploy_dev', 'SUCCESS')
		wsCleanup()
	}
}



job("30_peacock-hcm-adapter_v1_deploy_dev") {
	description("Deploys Basket Service on to DEV environment.")
	keepDependencies(false)
	disabled(false)
	concurrentBuild(false)
	label('docker-build')

	wrappers {
		preBuildCleanup {
			deleteDirectories(false)
			cleanupParameter()
		}
	}


	steps {
		copyArtifacts('10_peacock-hcm-adapter_v1_build_master') {
			includePatterns('**/task-definiton-fto*.json,**/*-service-fto*.json')
			flatten(true)
			buildSelector {
			}
		}
	}


	steps { shell("""#!/bin/bash
set +e

servicename=basket
targetenv=dev

aws ecs register-task-definition --cli-input-json file://task-definiton-fto-\${"servicename"}-AwsTask-\${"targetenv"}.json --profile nonprod-m-ordering
aws ecs update-service --cli-input-json file://scale-down-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering
aws ecs delete-service --cli-input-json file://delete-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering

command || true""") }

	steps { shell("""set -e

servicename=basket
targetenv=dev

sleep 60

aws ecs create-service --cli-input-json file://create-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering""") }

	configure {
		it / 'properties' / 'jenkins.model.BuildDiscarderProperty' {
			strategy {
				'daysToKeep'('3')
				'numToKeep'('3')
				'artifactDaysToKeep'('-1')
				'artifactNumToKeep'('-1')
			}
		}
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
		it / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
			projectNameList { string('10_peacock-hcm-adapter_v1_build_master') }
		}
	}

	publishers {
		downstream('40_peacock-hcm-adapter_v1_deploy_sit', 'SUCCESS')
		wsCleanup()
	}
}


job("40_peacock-hcm-adapter_v1_deploy_sit") {
	description("Deploys Basket Service on to SIT environment.")
	keepDependencies(false)
	disabled(false)
	concurrentBuild(false)
	label('docker-build')

	wrappers {
		preBuildCleanup {
			deleteDirectories(false)
			cleanupParameter()
		}
	}


	steps {
		copyArtifacts('10_peacock-hcm-adapter_v1_build_master') {
			includePatterns('**/task-definiton-fto*.json,**/*-service-fto*.json')
			flatten(true)
			buildSelector {
			}
		}
	}


	steps { shell("""#!/bin/bash
set -e

servicename=basket
targetenv=sit
sourceenv=dev
image=morrisons/\${"servicename"}-service:\${"sourceenv"};
docker pull \${"image"}

repository=morrisons/\${"servicename"}-service;

docker tag \${"image"} \${"repository"}:\${"targetenv"}
docker push \${"repository"}:\${"targetenv"}

#Remove DEV Image
docker rmi \${"image"}

#Remove SIT Image
docker rmi \${"repository"}:\${"targetenv"}""") }

	steps { shell("""set +e

servicename=basket
targetenv=sit

aws ecs register-task-definition --cli-input-json file://task-definiton-fto-\${"servicename"}-AwsTask-\${"targetenv"}.json --profile nonprod-m-ordering
aws ecs update-service --cli-input-json file://scale-down-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering
aws ecs delete-service --cli-input-json file://delete-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering

command || true""") }

	steps { shell("""#!/bin/bash
set -e

sleep 60

servicename=basket
targetenv=sit
aws ecs create-service --cli-input-json file://create-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering""") }

	configure {
		it / 'properties' / 'jenkins.model.BuildDiscarderProperty' {
			strategy {
				'daysToKeep'('3')
				'numToKeep'('3')
				'artifactDaysToKeep'('-1')
				'artifactNumToKeep'('-1')
			}
		}
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
		it / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
			projectNameList { string('10_peacock-hcm-adapter_v1_build_master') }
		}
	}

	publishers {
		downstream('50_peacock-hcm-adapter_v1_deploy_uat', 'SUCCESS')
		wsCleanup()
	}
}


job("50_peacock-hcm-adapter_v1_deploy_uat") {
	description("Deploys Basket Service on to UAT environment.")
	keepDependencies(false)
	disabled(false)
	concurrentBuild(false)
	label('docker-build')

	wrappers {
		preBuildCleanup {
			deleteDirectories(false)
			cleanupParameter()
		}
	}


	steps {
		copyArtifacts('10_peacock-hcm-adapter_v1_build_master') {
			includePatterns('**/task-definiton-fto*.json,**/*-service-fto*.json')
			flatten(true)
			buildSelector {
			}
		}
	}


	steps { shell("""#!/bin/bash
set -e

servicename=basket
targetenv=uat
sourceenv=sit
image=morrisons/\${"servicename"}-service:\${"sourceenv"};
docker pull \${"image"}

repository=morrisons/\${"servicename"}-service;

docker tag \${"image"} \${"repository"}:\${"targetenv"}
docker push \${"repository"}:\${"targetenv"}

#Remove SIT Image
docker rmi \${"image"}

#Remove UAT Image
docker rmi \${"repository"}:\${"targetenv"}""") }

	steps { shell("""set +e

servicename=basket
targetenv=uat

aws ecs register-task-definition --cli-input-json file://task-definiton-fto-\${"servicename"}-AwsTask-\${"targetenv"}.json --profile nonprod-m-ordering

aws ecs update-service --cluster nonprod-ECS-ECSCluster-W68V9ZHCS399 --service fto-\${"servicename"}-AwsService-\${"targetenv"} --desired-count 0 --profile nonprod-m-ordering

aws ecs delete-service --cluster nonprod-ECS-ECSCluster-W68V9ZHCS399 --service fto-\${"servicename"}-AwsService-\${"targetenv"} --profile nonprod-m-ordering""") }

	steps { shell("""#!/bin/bash
set -e

sleep 60

servicename=basket
targetenv=uat
aws ecs create-service --cli-input-json file://create-service-fto-\${"servicename"}-AwsService-\${"targetenv"}.json --profile nonprod-m-ordering""") }

	configure {
		it / 'properties' / 'jenkins.model.BuildDiscarderProperty' {
			strategy {
				'daysToKeep'('3')
				'numToKeep'('3')
				'artifactDaysToKeep'('-1')
				'artifactNumToKeep'('-1')
			}
		}
		it / 'properties' / 'com.sonyericsson.rebuild.RebuildSettings' {
			'autoRebuild'('false')
			'rebuildDisabled'('false')
		}
		it / 'properties' / 'hudson.plugins.copyartifact.CopyArtifactPermissionProperty' {
			projectNameList { string('10_peacock-hcm-adapter_v1_build_master') }
		}
	}

	publishers { wsCleanup() }
}

listView("FoodToOrder"){
	description('Food to order view for job DSL')
	filterBuildQueue()
	filterExecutors()
	jobs{
		name('10_peacock-hcm-adapter_v1_build_master')
		name('20_peacock-hcm-adapter_v1_build_container')
		name('30_peacock-hcm-adapter_v1_deploy_dev')
		name('40_peacock-hcm-adapter_v1_deploy_sit')
		name('50_peacock-hcm-adapter_v1_deploy_uat')
	}
	columns{
		status()
		weather()
		name()
		lastSuccess()
		lastFailure()
		lastDuration()
		buildButton()
		lastBuildConsole()
	}
	jobFilters {
		status { status(Status.STABLE) }
	}
}
