SHELL := /bin/bash
########################################################################################################################
##
## Makefile for managing peacock
##
########################################################################################################################
THIS_FILE := $(lastword $(MAKEFILE_LIST))
activate = VIRTUAL_ENV_DISABLE_PROMPT=true . .venv/bin/activate;

ensure-venv:
ifeq ($(wildcard .venv),)
	@$(MAKE) -f $(THIS_FILE) venv
endif

venv:
	if [ -d .venv ]; then rm -rf .venv; fi
	python3.6 -m venv .venv --clear
	$(activate) pip3 install --upgrade pip

init: ensure-venv
	$(activate) pip3 install -r requirements.txt

maven:
	mvn clean install

#Terraform
terraform-dev:
	$(MAKE) -C terraform/deployments/dev terraform-dev args="-auto-approve"

terraform-destroy:
	$(MAKE) -C terraform/deployments/dev terraform-destroy args="-auto-approve"

terraform-behave:
	$(activate) behave terraform/features/
