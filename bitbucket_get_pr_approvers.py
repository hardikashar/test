#!/usr/bin/env python
import argparse
import sys
from pybitbucket import pullrequest
from pybitbucket.auth import BasicAuthenticator


def validate_pull_request_approver(user_name: str, password: str, email: str, repository: str, owner: str, pull_id: int,
                                   approvers: list) -> bool:
    approved_count = 0
    bb_client = pullrequest.Client(BasicAuthenticator(user_name, password, email))
    pull_request = pullrequest.PullRequest. \
        find_pullrequest_by_id_in_repository(pull_id,
                                             repository_name=repository,
                                             owner=owner,
                                             client=bb_client)

    for participant in pull_request.participants:
        print(participant['approved'])
        print(participant['user']['username'])
        print(participant['user']['username'] in approvers)

        if participant['user']['username'] in approvers and participant['approved']:
            approved_count = approved_count + 1
            if approved_count == len(approvers):
                return True

    return False


def main():
    parser = argparse.ArgumentParser(description='Validate pull requests approver '
                                                 'list for Jenkins. Merge build only if approved by valid approver')

    parser.add_argument(
        '--user_name', type=str, help='Username of bitbucket'
    )

    parser.add_argument(
        '--password', type=str, help='password of bitbucket'
    )

    parser.add_argument(
        '--repository', type=str, help='repository of bitbucket'
    )

    parser.add_argument(
        '--email', type=str, help='repository of bitbucket'
    )

    parser.add_argument(
        '--owner', type=str, help='owner repository of bitbucket'
    )

    parser.add_argument(
        '--pull_id', type=str, help='ID of Pull request'
    )
    parser.add_argument(
        '--approvers', nargs='+', help='List of valid approvers for pull request'
    )

    args = parser.parse_args()

    ret = validate_pull_request_approver(args.user_name,
                                         args.password,
                                         args.email,
                                         args.repository,
                                         args.owner,
                                         args.pull_id,
                                         args.approvers)
    if ret:
        print('Success')
    else:
        print('Failure')
        sys.exit(1)

    # sys.exit(0)


if __name__ == "__main__":
    main()
