package com.morrisons.peacock.hcm.adapter.client;

import com.amazonaws.SdkClientException;
import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.AmazonS3ClientBuilder;
import com.amazonaws.services.s3.model.AmazonS3Exception;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.PutObjectResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class S3ClientImpl implements S3Client {

    private AmazonS3 s3Client;
    private Logger logger = LoggerFactory.getLogger(this.getClass());

//    private final String AWS_ACCESS_KEY_ID = "AKIAJ5GH3YTF7G47FQNQ";
//    private final String AWS_SECRET_KEY = "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";
    private final String bucketName = "peacock-poc-hcm-upload";
    private final String stringObjKeyName = "test.txt";

    public S3ClientImpl() {
//        AWSCredentials credentials = new BasicAWSCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);
//
//        AWSCredentialsProvider credentialProvider = new AWSCredentialsProvider() {
//            @Override
//            public AWSCredentials getCredentials() {
//                return credentials;
//            }
//
//            @Override
//            public void refresh() {
//
//            }
//        };
//        s3Client = AmazonS3ClientBuilder.standard()
//                .withRegion(Regions.valueOf("EU_WEST_1"))
//                .withCredentials(credentialProvider).build();

                s3Client = AmazonS3ClientBuilder.standard()
                .withRegion(Regions.valueOf("EU_WEST_1")).build();
    }

    public AmazonS3 getAmazonS3Client() {
        return s3Client;
    }

    public Map<String, String> getDetails (String bucketName, String fileName){

        System.out.println(" *********** Inside S3ClientImpl.getDetails method *********** ");

        System.out.println("bucket name passed to this method is = " + bucketName);

        try {

////            PutObjectResult putObjectResult = s3Client.putObject(bucketName, stringObjKeyName, new File(fileName));
////
////            logger.info("putObjectResult = " + putObjectResult.toString());
////            System.out.println("putObjectResult = " + putObjectResult.toString());
//
//
////            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, fileName, new File("/tmp/test.txt"));
////            s3Client.putObject(putObjectRequest);
//
//            File file = File.createTempFile("test2-temp-file", "txt");
//            file.setWritable(true);
//
//            ObjectMetadata metadata = new ObjectMetadata();
//            metadata.setContentType("text/plain");
//
//            Map<String, String> userMedata = new HashMap<String, String>();
//            userMedata.put("x-amz-meta-headerkey1","header-val-1");
//            userMedata.put("x-amz-meta-headerkey2","header-val-2");
//
//            metadata.setUserMetadata(userMedata);
//
//            System.out.println("********** user metadata set *********************");
//            PutObjectRequest putObjectRequest = new PutObjectRequest(bucketName, "test2-temp-file.txt", file);
//            putObjectRequest.withMetadata(metadata);
//
//            PutObjectResult putObjectResult = s3Client.putObject(putObjectRequest);
//
//            logger.info("S3ClientImpl: putObjectResult " + putObjectResult.toString());
//            System.out.println("S3ClientImpl: putObjectResult " + putObjectResult.toString());

            // to get user defined metadata
//            final ObjectMetadata objectListing = s3Client.getObjectMetadata(bucketName, "test2-temp-file.txt" );
            final ObjectMetadata objectListing = s3Client.getObjectMetadata(bucketName, fileName);
            final Map<String, String> userMetadata = objectListing.getUserMetadata();
            System.out.println(" **************************************** ");
            userMetadata.forEach((k,v)-> System.out.println("Key: " + k + ": Value: " +v));
            System.out.println(" **************************************** ");

            return userMetadata;


        }
        catch (AmazonS3Exception s3exception){
            logger.error("Exception caught in S3ClientImpl " + s3exception);
            s3exception.printStackTrace();
        } catch (SdkClientException sdkClientException){
            logger.error("Exception caught in S3ClientImpl " + sdkClientException);
            sdkClientException.printStackTrace();
        }

        //TODO - correct this.
        return new HashMap<String, String>();
    }



}
