package com.morrisons.peacock.hcm.adapter.poc.lambda;

import com.amazonaws.services.lambda.runtime.Context;
import com.amazonaws.services.lambda.runtime.RequestHandler;
import com.amazonaws.services.lambda.runtime.events.S3Event;
import com.amazonaws.services.s3.event.S3EventNotification;

import com.morrisons.peacock.hcm.adapter.client.ECSClient;
import com.morrisons.peacock.hcm.adapter.client.ECSClientImpl;
import com.morrisons.peacock.hcm.adapter.client.S3Client;
import com.morrisons.peacock.hcm.adapter.client.S3ClientImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.Map;

//public class PocHCMAdapterLambda { //implements RequestHandler<Object,Object> {
public class PocHCMAdapterLambda implements RequestHandler<S3Event,String> {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private S3Client s3Client;

    private ECSClient ecsClient;

    private FileContext fileContext;

    public PocHCMAdapterLambda() {
        s3Client = new S3ClientImpl();
        ecsClient = new ECSClientImpl();
        fileContext = new FileContext();
    }

    //@Override
//    public Object handleRequest(HashMap request, Context context) {
    public String handleRequest(S3Event s3event, Context context) {
        try {

            System.out.println("from sysout - Inside PocHCMAdapterLambda.handleRequest lambda function");
            logger.info("-- from logger - inside PocHCMAdapterLambda.handleRequest lambda function");
            context.getLogger().log("from CONTEXT logger - inside lambda function");

            String req = null;


        s3event.getRecords().stream().forEach((k)-> {
            System.out.println("%%%%%%%%%%%%%%%%%%%%%");
            System.out.println("Key: " + k);
            System.out.println("%%%%%%%%%%%%%%%%%%%%%");
        });

            printS3Event(s3event, fileContext);

            System.out.println("fileContext.getArn() : " + fileContext.getArn());
            System.out.println("fileContext.getBucketName() : " + fileContext.getBucketName());
            System.out.println("fileContext.getFileName() :" + fileContext.getFileName());

            // TODO - add s3Client and call getObjectMetadata method to get user defined metadata.
            final Map<String, String> metadataDetails = s3Client.getDetails(fileContext.getBucketName(), fileContext.getFileName());

            fileContext.setObjectMetadata(metadataDetails);
            // TODO - call ecsClient to pass these details to take further action.
            ecsClient.runTask("upload", fileContext);

        } catch (Exception ex){
            ex.printStackTrace();
        }

        return "from hcm adapter lambda";

    }

    private void printS3Event (S3Event s3event, FileContext valueHolder){

        System.out.println(" %%%%%%%%%%%%%%%%%% Inside printS3Event %%%%%%%%%%%%%%%%%%%%%%");

        S3EventNotification.S3EventNotificationRecord record = s3event.getRecords().get(0);

        System.out.println("record = " + record.toString());

        // Retrieve the bucket & key for the uploaded S3 object that
        // caused this Lambda function to be triggered
        String bkt = record.getS3().getBucket().getName();
        System.out.println("bkt = " + bkt);
        String arn = record.getS3().getBucket().getArn();
        System.out.println("arn = " + arn);
        String key = record.getS3().getObject().getKey().replace('+', ' ');
        System.out.println("key = " + key);

        final S3EventNotification.S3ObjectEntity object = record.getS3().getObject();
        System.out.println("object = " + object.toString());

        valueHolder.setArn(arn);
        valueHolder.setBucketName(bkt);
        valueHolder.setFileName(key);


        try {
            key = URLDecoder.decode(key, "UTF-8");
            System.out.println("key = " + key);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        // Read the source file as text
//        AmazonS3 s3Client = AmazonS3ClientBuilder.defaultClient();
        String body = s3Client.getAmazonS3Client().getObjectAsString(bkt, key);
        System.out.println("Body: " + body);


    }

}

