package com.morrisons.peacock.hcm.adapter.client;

import com.amazonaws.services.s3.AmazonS3;

import java.util.Map;

public interface S3Client {

    Map<String, String> getDetails(String bucketName, String file);

    AmazonS3 getAmazonS3Client();
}
