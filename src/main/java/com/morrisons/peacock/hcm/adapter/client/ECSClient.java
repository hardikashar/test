package com.morrisons.peacock.hcm.adapter.client;

import com.amazonaws.services.ecs.model.RunTaskResult;
import com.morrisons.peacock.hcm.adapter.poc.lambda.FileContext;

public interface ECSClient {

    RunTaskResult runTask(String operation, FileContext uploadContext);
}
