package com.morrisons.peacock.hcm.adapter.poc.lambda;

import java.util.Map;

public class FileContext {
    String operation;
    String bucketName;
    String fileName;
    String arn;
    String integrationId;
    Map<String, String> objectMetadata;

    public String getOperation() {
        return operation;
    }

    public void setOperation(String operation) {
        this.operation = operation;
    }

    public String getBucketName() {
        return bucketName;
    }

    public void setBucketName(String bucketName) {
        this.bucketName = bucketName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getArn() {
        return arn;
    }

    public void setArn(String arn) {
        this.arn = arn;
    }


    public String getIntegrationId() {
        return integrationId;
    }

    public void setIntegrationId(String integrationId) {
        this.integrationId = integrationId;
    }

    public Map<String, String> getObjectMetadata() {
        return objectMetadata;
    }

    public void setObjectMetadata(Map<String, String> objectMetadata) {
        this.objectMetadata = objectMetadata;
    }
}
