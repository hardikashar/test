package com.morrisons.peacock.hcm.adapter.poc.lambda;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.BufferedReader;
import java.io.Writer;

import com.amazonaws.services.ecs.model.RunTaskResult;
import com.amazonaws.services.lambda.runtime.RequestStreamHandler;
import com.amazonaws.services.lambda.runtime.Context; 
import com.amazonaws.services.lambda.runtime.LambdaLogger;


import com.morrisons.peacock.hcm.adapter.client.ECSClientImpl;
import org.apache.http.protocol.HTTP;
import org.json.simple.JSONObject;
import org.json.simple.JSONArray;
import org.json.simple.parser.ParseException;
import org.json.simple.parser.JSONParser;



public class ProxyWithStream implements RequestStreamHandler {
    JSONParser parser = new JSONParser();

    private FileContext fileContext;
    private ECSClientImpl ecsClient;
    private static String successResponseCode = "200";

    public ProxyWithStream() {
        this.fileContext = new FileContext();
        this.ecsClient = new ECSClientImpl();
    }

    public void handleRequest(InputStream inputStream, OutputStream outputStream, Context context) throws IOException {

        LambdaLogger logger = context.getLogger();
        logger.log("Loading Java Lambda handler of ProxyWithStream");
        System.out.println("Loading Java Lambda handler of ProxyWithStream");


        // processRequest(inputStream, outputStream, logger);

        processRequestV2(inputStream, outputStream, logger);

        return;
    }

    private void processRequestV2(InputStream inputStream, OutputStream outputStream, LambdaLogger logger) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();

        System.out.println("processRequestV2:reader = "  + reader.toString());

        // get data from the request

        try {
            JSONObject event = (JSONObject)parser.parse(reader);
//            if (event.get("queryStringParameters") != null) {
//                JSONObject qps = (JSONObject)event.get("queryStringParameters");
//                if ( qps.get("name") != null) {
//                    name = (String)qps.get("name");
//                }
//            }
//
//            if (event.get("pathParameters") != null) {
//                JSONObject pps = (JSONObject)event.get("pathParameters");
//                if ( pps.get("proxy") != null) {
//                    city = (String)pps.get("proxy");
//                }
//            }
//
//            if (event.get("headers") != null) {
//                JSONObject hps = (JSONObject)event.get("headers");
//                if ( hps.get("day") != null) {
//                    day = (String)hps.get("day");
//                }
//            }

            if (event.get("body") != null) {
                JSONObject body = (JSONObject)parser.parse((String)event.get("body"));

                if (body.get("operation") != null){
                    String operation = (String) body.get("operation");
                    logger.log("operation passed is : " + operation);
                    System.out.println("operation passed is : " + operation);
                    fileContext.setOperation(operation);

                }

                if (body.get("integrationId") != null){
                    String integrationId = (String) body.get("integrationId");
                    logger.log("integrationId passed is : " + integrationId);
                    System.out.println("integrationId passed is : " + integrationId);
                    fileContext.setIntegrationId(integrationId);

                }
            }


            // TODO - call ECS task for requesting an extract
            final RunTaskResult runTaskResult = ecsClient.runTask("upload", fileContext);

            // set details in response
            JSONObject responseBody = new JSONObject();
            responseBody.put("input", event.toJSONString());
            responseBody.put("runTaskResult", runTaskResult);

            JSONObject headerJson = new JSONObject();
            headerJson.put("x-custom-header", "my custom header value");

            responseJson.put("isBase64Encoded", false);
            responseJson.put("statusCode", successResponseCode);
            responseJson.put("headers", headerJson);
            responseJson.put("body", responseBody.toString());

        } catch(ParseException pex) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", pex);
        }





        logger.log(responseJson.toJSONString());
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());

        System.out.println("Output is = " + responseJson.toJSONString());

        writer.close();
    }

    private void processRequest(InputStream inputStream, OutputStream outputStream, LambdaLogger logger) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        JSONObject responseJson = new JSONObject();

        System.out.println("reader = "  + reader.toString());

        String name = "you";
        String city = "World";
        String time = "day";
        String day = null;
        String responseCode = "200";

        try {
            JSONObject event = (JSONObject)parser.parse(reader);
            if (event.get("queryStringParameters") != null) {
                JSONObject qps = (JSONObject)event.get("queryStringParameters");
                if ( qps.get("name") != null) {
                    name = (String)qps.get("name");
                }
            }

            if (event.get("pathParameters") != null) {
                JSONObject pps = (JSONObject)event.get("pathParameters");
                if ( pps.get("proxy") != null) {
                    city = (String)pps.get("proxy");
                }
            }

            if (event.get("headers") != null) {
                JSONObject hps = (JSONObject)event.get("headers");
                if ( hps.get("day") != null) {
                    day = (String)hps.get("day");
                }
            }

            if (event.get("body") != null) {
                JSONObject body = (JSONObject)parser.parse((String)event.get("body"));
                if ( body.get("time") != null) {
                    time = (String)body.get("time");
                }
            }

            String greeting = "Good " + time + ", " + name + " of " + city + ". ";
            if (day!=null && day != "") greeting += "Happy " + day + "!";


            JSONObject responseBody = new JSONObject();
            responseBody.put("input", event.toJSONString());
            responseBody.put("message", greeting);

            JSONObject headerJson = new JSONObject();
            headerJson.put("x-custom-header", "my custom header value");

            responseJson.put("isBase64Encoded", false);
            responseJson.put("statusCode", responseCode);
            responseJson.put("headers", headerJson);
            responseJson.put("body", responseBody.toString());

        } catch(ParseException pex) {
            responseJson.put("statusCode", "400");
            responseJson.put("exception", pex);
        }

        logger.log(responseJson.toJSONString());
        OutputStreamWriter writer = new OutputStreamWriter(outputStream, "UTF-8");
        writer.write(responseJson.toJSONString());

        System.out.println("Output is = " + responseJson.toJSONString());

        writer.close();
    }
}