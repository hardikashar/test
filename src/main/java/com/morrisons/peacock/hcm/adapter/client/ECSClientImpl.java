package com.morrisons.peacock.hcm.adapter.client;

import com.amazonaws.auth.AWSCredentials;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.ecs.AmazonECSClient;
import com.amazonaws.services.ecs.model.*;
import com.morrisons.peacock.hcm.adapter.poc.lambda.FileContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class ECSClientImpl implements ECSClient {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

//    private final String AWS_ACCESS_KEY_ID = "AKIAJ5GH3YTF7G47FQNQ";
//    private final String AWS_SECRET_KEY = "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";

    private final AmazonECSClient amazonECSClient;

    /**
     * Constructor
     */
    public ECSClientImpl() {
        logger.info("Inside ECSClientImpl constructor");
        // set credential to access ECS task from AWS Lambda
//        AWSCredentials credentials = new BasicAWSCredentials(AWS_ACCESS_KEY_ID, AWS_SECRET_KEY);

        // AWS ECS Client initialisation
//        this.amazonECSClient = new AmazonECSClient(credentials);
        this.amazonECSClient = new AmazonECSClient();

        this.amazonECSClient.setRegion(Region.getRegion(Regions.valueOf("EU_WEST_1")));
    }

    @Override
    public RunTaskResult runTask(String operation, FileContext fileContext) {

        logger.info(" ----------- Inside ECSClientImpl.runTask method. -----------");
        System.out.println(" ----------- Inside ECSClientImpl.runTask method. -----------");

//        List<KeyValuePair> keyValuePairs = new ArrayList<>();
//
//        KeyValuePair accessKey = new KeyValuePair();
//        accessKey.setName("AWS_ACCESS_KEY_ID");
//        accessKey.setValue(AWS_ACCESS_KEY_ID);
//
//        KeyValuePair secretKey = new KeyValuePair();
//        secretKey.setName("AWS_SECRET_KEY");
//        secretKey.setValue(AWS_SECRET_KEY);
//
//        keyValuePairs.add(accessKey);
//        keyValuePairs.add(secretKey);


        // container overrides
        ContainerOverride containerOverride = new ContainerOverride();
//        containerOverride.setEnvironment(keyValuePairs);
        containerOverride.setName("peacock-ecs-container"); //peacock-ecs-container");
        Collection<String> arCmdString = new ArrayList<>();
        // Since this lambda will be invoked on back of S3 PUT event, this always will be an extract.

        //TODO - get the details about the S3 file additional metadata and pass the same to task.
        // filename
        // version ?
        // extract name / ID - which may be part of extract file or that need to be set when file has been
        // written to S3 bucket.

        if (fileContext.getObjectMetadata() != null) {
            fileContext.getObjectMetadata().forEach((k, v) -> {
                System.out.println("poc-hcm-adapter-labda - ECSClientImpl - $$$$$$$$  Key = " + k + " Value: " + v);
            });
        }

        if (fileContext.getOperation() != null){
            arCmdString.add(fileContext.getOperation());
            // put check for null
            arCmdString.add(fileContext.getIntegrationId());
        }else {
            arCmdString.add("upload");


//        arCmdString.add("fileName=abc.txt");
//        arCmdString.add("fileVersion=1.1");
//        arCmdString.add("integrationID=101");
            arCmdString.add("fileName=" + fileContext.getFileName());
            arCmdString.add("integrationID=" + fileContext.getObjectMetadata().get("x-amz-meta-integration-id"));
            arCmdString.add("fileAddInfo=" + fileContext.getObjectMetadata().get("x-amz-meta-file-add-info"));
            arCmdString.add("bucketName=" + fileContext.getBucketName());

        }

        containerOverride.setCommand(arCmdString);

        List<ContainerOverride> containerOverrides = new ArrayList<>();
        containerOverrides.add(containerOverride);

        TaskOverride taskOverride = new TaskOverride();
        taskOverride.withContainerOverrides(containerOverrides);

        RunTaskRequest runTaskRequest = new RunTaskRequest();
        runTaskRequest.withTaskDefinition("peacock-ecs-poc-hcm-task"); // previously it was peacock-ecs-poc-task
        runTaskRequest.withOverrides(taskOverride);
        runTaskRequest.setCluster("peacock-ecs-cluster");

        return this.amazonECSClient.runTask(runTaskRequest);

    }

}
